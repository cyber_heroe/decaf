#include <cstdio>
#include <iostream>
#include <string>
#include "ast.h"
#include "tokens.h"

extern Statement *input;
int yyparse();

std::map<string, Statement>::iterator itFunc;

int main()
{
    input = 0;
    yyparse();
    
    if (input != 0) {

        try{
            itFunc = functions.find("main");
            if (itFunc != functions.end()){
                Statement *main = *itFunc;
                main->evaluate();
            }else{
                throw DecafException("No esta declarado el Main");
            }
        }catch(DecafException& e){
            cout << e.what() << endl;
        }


        input->execute();
    }
}
