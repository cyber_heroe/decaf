%{
#include <cstdio>
#include <string>
#include "ast.h"

using namespace std;

int yylex();

void yyerror(const char *str)
{
    printf("%s\n", str);
}

#define YYERROR_VERBOSE 1

Statement *input;

%}

%union {
    char *id_t;
    int num_t;
    Statement *statement_t;
    Expr *expr_t;
    list<Expr *> exprList_t;
    DataType type_t;
    Param *param_t;
    list<Param *> paramList_t;
}

%expect 1

%token<num_t> NUM_I NUM_H
%token<id_t> ID STRING_LITERAL
%token RW_BOOL RW_BREAK RW_PRINT RW_READ RW_CONTINUE RW_CLASS RW_ELSE
%token RW_EXTENDS RW_FALSE RW_FOR RW_IF RW_INT RW_NEW RW_NULL
%token RW_RETURN RW_ROT RW_TRUE RW_VOID RW_WHILE
%token OP_LT OP_GT OP_LTE OP_GTE OP_NE OP_EQ OP_AND OP_OR
%type<expr_t> expra expr term factor class_name argument
%type<exprList_t> arg_list
%type<statement_t> list_st st opt_else block assign program decl_list decl
%type<param_t> param
%type<paramList_t> param_list
%type<type_t> type type_op

%%

input: program { input = $1; }
;

program: RW_CLASS class_name '{' decl_list  '}' { $$ = $4; }
;

class_name: ID { $$ = $1; }
;

decl_list: decl_list decl {
                            if ($1->getKind() == BLOCK_STATEMENT) {
                                BlockStatement *block = (BlockStatement*)$1;
                                block->stList.push_back($2);
                                $$ = block;
                            } else {
                                list<Statement *> l;
                                
                                l.push_back($1);
                                l.push_back($2);
                                
                                $$ = new BlockStatement(l);
                            }
                          }
        |  decl { $$ = $1; }
;

decl: RW_VOID ID '(' param_list ')' block { $$ = new VoidStatement($2, $6, $4); }
    | type ID '(' param_list ')' block { $$ = new FunctionStatement($2, $6, $1, $4); }
    | type ID ';' { $$ = new DeclarationStatement($2, $1); }
    | type ID '=' expr ';' { $$ = new DeclarationStatement($2, $1, $4); }
;

param_list: param_list ',' param {
                                list<Param *> l;
                                l.push_back($1);
                                l.push_back($3);
                                $$ = l;
                             }
        | param { $$ = $1; }
;

param: type ID ';' { $$ = new Param($2, $1); }
    | type ID '=' expr ';' { $$ = new Param($2, $1, $4); }
;

list_st: list_st st { 
                        if ($1->getKind() == BLOCK_STATEMENT) {
                            BlockStatement *block = (BlockStatement*)$1;
                            
                            block->stList.push_back($2);
                            
                            $$ = block;
                        } else {
                            list<Statement *> l;
                            
                            l.push_back($1);
                            l.push_back($2);
                            
                            $$ = new BlockStatement(l);
                        }
                    }
                    
        | st        { $$ = $1; }
;

st: RW_PRINT arg_list ';' { $$ = new PrintStatement($2); delete $2; }
    |type ID '=' expr ';'{ 
                          string id = $2;
                          
                          free($2);
                          
                          $$ = new AssignStatement(id, $4,$1);
                         } 
    | RW_IF '(' expr ')' block opt_else    { $$ = new IfStatement($3, $5, $6); }
    | RW_WHILE '(' expr ')' block { $$ = new WhileStatement($3, $5); }   
    | RW_FOR '(' assign ';' expr ';' assign ')' block { $$ = new ForStatement($3, $5, $7,$9); }
    | type ID '+' '+'   { 
                          string id = $2;
                          
                          free($2);
                          
                          $$ = new IncStatement(id);
                        }
    | type ID '-' '-'   { 
                          string id = $2;
                          
                          free($2);
                          
                          $$ = new DecStatement(id);
                        } 

;

arg_list: arg_list ',' argument {
        $$ = $1;
        $$->push_back($3);
      }
    | argument { 
        $$ = new list<Expr *>();
        $$->push_back($1);
      }
;


argument: expr { $$ = $1; }
    | STRING_LITERAL {
        string str = $1;
                            
        free($1);
            $$ = new StringExpr(str);
    }
;


opt_else: RW_ELSE block    { $$ = $2; }
        |               { $$ = 0; }
;

block: '{' list_st '}'  { $$ = $2; }
;

assign:  type_op ID '=' expr   { 
                          string id = $2;
                          
                          free($2);
                          
                          $$ = new AssignStatement(id, $4,$1);
                        }
                                           
        | type_op ID '+' '+'     { 
                          string id = $2;
                          
                          free($2);
                          
                          $$ = new IncStatement(id);
                        } 
                        
    | type_op ID '-' '-'     { 
                          string id = $2;
                          
                          free($2);
                          
                          $$ = new DecStatement(id);
                        }     
;

type_op: type { $$ = $1; }
        | { $$ = 0; }
;

type: RW_INT  { $$ = DT_INT; }
     | RW_BOOL { $$ = DT_BOOL; }
;



expr: expr OP_LT expra  { $$ = new LTExpr($1, $3); }
    | expr OP_GT expra  { $$ = new GTExpr($1, $3); }
    | expr OP_LTE expra { $$ = new LTEExpr($1, $3); }
    | expr OP_GTE expra { $$ = new GTEExpr($1, $3); }
    | expr OP_NE expra  { $$ = new NEExpr($1, $3); }
    | expr OP_EQ expra  { $$ = new EQExpr($1, $3); }
    | expra             { $$ = $1; }
;

expra: expra '+' term   { $$ = new AddExpr($1, $3); }
    | expra '-' term    { $$ = new SubExpr($1, $3); }
    | term              { $$ = $1; }
;

term: term '*' factor   { $$ = new MultExpr($1, $3); }
    | term '/' factor   { $$ = new DivExpr($1, $3); }
    | term '%' factor   { $$ = new ModExpr($1, $3); }
    | factor            { $$ = $1; }
;

factor: '(' expr ')'    { $$ = $2; }
        | NUM_I         { $$ = new NumExpr($1); }
        | NUM_H         { $$ = new NumExpr($1, true); }
        | RW_TRUE       { $$ = new BoolExpr(true); }
        | RW_FALSE      { $$ = new BoolExpr(false); }
        | ID            { 
                            string id = $1;
                            
                            free($1);
                            $$ = new IdExpr(id);
                        }
;

%%
