%option noyywrap

%{
#include <cstdlib>
#include <cstring>
#include "ast.h"
#include "tokens.h"

int line = 0;

%}

DIGIT [0-9]
LETTER [a-zA-Z]
SYMBOL [_]
PREHEXA [0x]
HEXALETTER [a-fA-F]
%%

"{" { return '{'; }
"}" { return '}'; }
"[" { return '['; }
"]" { return ']'; }
"." { return '.'; }
";" { return ';'; 
"(" { return '('; }
")" { return ')'; }
"=" { return '='; }
"-" { return '-'; }
"!" { return '!'; }
"+" { return '+'; }
"*" { return '*'; }
"/" { return '/'; }
"<<" { return "<<"; }
">>" { return ">>"; }
"<" { return OP_LT; }
">" { return OP_GT; }
"%" { return OP_MOD; }
"<=" { return OP_LTE; }
">=" { return OP_GTE; }
"==" { return OP_EQ; }
"!=" { return OP_NE; }
"&&" { return OP_AND; }
"||" { return OP_OR; }
"." { return '.'; }
[ \t] /* Nada */
\n  { line++; }
"bool" { return RW_BOOL; }
"break" { return RW_BREAK; }
"print" { return RW_PRINT; }
"read" { return RW_READ; }
"continue" { return RW_CONTINUE; }
"class" { return RW_CLASS; }
"else"  { return RW_ELSE; }
"extends"  { return RW_EXTENDS; }
"false"  { return RW_FALSE; }
"for"  { return RW_FOR; }
"if"    { return RW_IF; }
"int"  { return RW_INT; }
"new"  { return RW_NEW; }
"null"  { return RW_NULL; }
"return"  { return RW_RETURN; }
"rot"  { return RW_ROT; }
"true"  { return RW_TRUE; }
"void"  { return RW_VOID; }
"while"  { return RW_WHILE; }
{DIGIT}+ { yylval.num_t = atoi(yytext); return NUM_I; }
{PREHEXA}({DIGIT}|{HEXALETTER})* { yylval.num_t = atoi(yytext) return NUM_H; }
({LETTER}|{SYMBOL})? {LETTER}({DIGIT}|{LETTER}|{SYMBOL})* { yylval.id_t = strdup(yytext); return ID; }
"'"[^']*"'" { yytext[yyleng-1] = '\0'; yylval.id_t = strdup(&yytext[1]); return STRING_LITERAL; }
"//"[^\n]*
.   { printf("Simbolo no valido\n"); }

%%
