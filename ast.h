#ifndef _AST_H_
#define _AST_H_

#include <string>
#include <list>
#include <map>
#include "decafexception.h"

using namespace std;

enum DataType {
    DT_INT,
    DT_BOOL,
    DT_STRING,
    DT_CHAR,
    DT_HEXA
};

struct Vvalue
{
    DataType type;

    union
    {
        int ivalue;
        bool bvalue;
        char cvalue;
        char* svalue;
    } u;

    bool bvalue()
    {
        if(type == DT_BOOL)
            return u.bvalue;
        else
            throw DecafException("Error de Casteo, tipo -> bool");
            //return (bool)ivalue;
    }

    int ivalue()
    {
        if(type == DT_INT || type == DT_HEXA)
            return u.ivalue;
        else if(type == DT_BOOL)
            return u.bvalue ? 1 : 0;
        else if(type == DT_CHAR)
            throw DecafException("Error de Casteo, char -> int");
        else if(type == DT_STRING)
            throw DecafException("Error de Casteo, string -> int");
    }

    char cvalue()
    {
        if(type == DT_CHAR)
            return u.cvalue;
        else
            throw DecafException("Error de Casteo, tipo -> char");
    }

    char* svalue()
    {
        if(type == DT_STRING)
            return u.svalue;
        else
            throw DecafException("Error de Casteo, tipo -> string");
    }
};

class Statement;
extern map<string, Vvalue> Globalvars;
extern map<string, Vvalue> vars;
extern map<string, Vvalue> varsDataType;
extern map<string, Statement*> functions;

class Param;
typedef list<Param *> Parameters;
typedef map<string, Vvalue> varsLocal;

enum StatementKind {
    BLOCK_STATEMENT,
    PRINT_STATEMENT,
    ASSIGN_STATEMENT,
    IF_STATEMENT,
    WHILE_STATEMENT,
    FOR_STATEMENT,
    INC_STATEMENT,
    DEC_STATEMENT,
    FUNCTION_STATEMENT,
    VOID_STATEMENT,
    DECLARATION_STATEMENT
};

enum ExprKind {
  LT_EXPR,
  LTE_EXPR,
  GT_EXPR,
  GTE_EXPR,
  NE_EXPR,
  EQ_EXPR,
  ADD_EXPR,
  SUB_EXPR,
  MULT_EXPR,
  DIV_EXPR,
  MOD_EXPR,
  NUM_EXPR,
  ID_EXPR,
  STRING_EXPR,
  BLOCK_EXPR
};

class Expr;
typedef list<Expr*> ExprList;

class Expr {
public:
    virtual Vvalue evaluate() = 0;
};

class BinaryExpr: public Expr {
public:
    BinaryExpr(Expr *expr1, Expr *expr2) {
        this->expr1 = expr1;
        this->expr2 = expr2;
    }

    Expr *expr1;
    Expr *expr2;
};

class LTExpr: public BinaryExpr {
public:
    LTExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class GTExpr: public BinaryExpr {
public:
    GTExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class LTEExpr: public BinaryExpr {
public:
    LTEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class GTEExpr: public BinaryExpr {
public:
    GTEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class NEExpr: public BinaryExpr {
public:
    NEExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class EQExpr: public BinaryExpr {
public:
    EQExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class AddExpr: public BinaryExpr {
public:
    AddExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class SubExpr: public BinaryExpr {
public:
    SubExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class MultExpr: public BinaryExpr {
public:
    MultExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class DivExpr: public BinaryExpr {
public:
    DivExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class ModExpr: public BinaryExpr {
public:
    ModExpr(Expr *expr1, Expr *expr2): BinaryExpr(expr1, expr2) {}

    Vvalue evaluate();
};

class NumExpr: public Expr {
public:
    NumExpr(int value, bool hexa = false)
    {
        this->value.type = hexa ? DT_HEXA : DT_INT;
        this->value.u.ivalue = value;
    }
    Vvalue evaluate() { return value; }

private:
    Vvalue value;
};

class BoolExpr: public Expr {
public:
    BoolExpr(bool value)
    {
        this->value.type = DT_BOOL;
        this->value.u.bvalue = value;
    }
    Vvalue evaluate() { return value; }

private:
    Vvalue value;
};

class StringExpr: public Expr {
public:
    StringExpr(char* str)
    {
        this->str = str;
        this->value.type = DT_STRING;
        this->value.u.svalue = str;
    }
    ExprKind getKind() { return STRING_EXPR; }

    Vvalue evaluate() { return value; }

private:
    string str;
    Vvalue value;
};


class IdExpr: public Expr {
public:
    IdExpr(string id) { this->id = id; }
    Vvalue evaluate(); //{ return vars[id]; }

    string id;
};

class BlockExpr: public Expr {
public:
    BlockExpr(list<Expr *> exprList) { this->exprList = exprList; }
    Vvalue evaluate() { Vvalue value; return value; }
    ExprKind getKind() { return BLOCK_EXPR; }

    list<Expr *> exprList;
};



class Statement {
public:
    virtual void execute() = 0;
    virtual StatementKind getKind() = 0;
};

class BlockStatement: public Statement {
public:
    BlockStatement(list<Statement *> stList) { this->stList = stList; }
    void execute();
    StatementKind getKind() { return BLOCK_STATEMENT; }

    list<Statement *> stList;
};

class PrintStatement: public Statement {
public:
    PrintStatement(ExprList *lexpr) { this->lexpr = lexpr; }
    void execute();
    StatementKind getKind() { return PRINT_STATEMENT; }

    ExprList *lexpr;
};

class AssignStatement: public Statement {
public:
    AssignStatement(string id, Expr *expr, DataType type) {
        this->id = id;
        this->expr = expr;
        this->type = type;
    }
    void execute();
    StatementKind getKind() { return ASSIGN_STATEMENT; }

    string id;
    Expr *expr;
    DataType type;
};


class IfStatement: public Statement {
public:
    IfStatement(Expr *cond, Statement *trueBlock, Statement *falseBlock) {
        this->cond = cond;
        this->trueBlock = trueBlock;
        this->falseBlock = falseBlock;
    }
    void execute();
    StatementKind getKind() { return IF_STATEMENT; }

    Expr *cond;
    Statement *trueBlock;
    Statement *falseBlock;
};

class WhileStatement: public Statement {
public:
    WhileStatement(Expr *cond, Statement *statementBlock) {
        this->cond = cond;
        this->statementBlock = statementBlock;
    }
    void execute();
    StatementKind getKind() { return WHILE_STATEMENT; }

    Expr *cond;
    Statement *statementBlock;
};

class ForStatement: public Statement {
public:
    ForStatement(Statement *assignStatement, Expr *cond,Statement *finalAssignStatement, Statement *statementBlock) {
        this->assignStatement=assignStatement;
        this->cond = cond;
        this->finalAssignStatement=finalAssignStatement;
        this->statementBlock = statementBlock;
    }
    void execute();
    StatementKind getKind() { return FOR_STATEMENT; }

    Statement *assignStatement;
    Expr *cond;
    Statement *finalAssignStatement;
    Statement *statementBlock;
};

class IncStatement: public Statement {
public:
    IncStatement(string id) {
        this->id = id;
    }
    void execute();
    StatementKind getKind() { return INC_STATEMENT; }

    string id;
};

class DecStatement: public Statement {
public:
    DecStatement(string id) {
        this->id = id;
    }
    void execute();
    StatementKind getKind() { return DEC_STATEMENT; }

    string id;
};

class FunctionStatement: public Statement {
public:
    FunctionStatement(string name, Statement *statementBlock, DataType type, Parameters *parameters);
    void execute();
    StatementKind getKind() { return FUNCTION_STATEMENT; }


    string name;
    Statement *statementBlock;
    DataType type;
    Parameters *parameters;
};

class VoidStatement: public Statement {
public:
    VoidStatement(string name, Statement *statementBlock, Parameters *parameters);
    void execute();
    StatementKind getKind() { return VOID_STATEMENT; }


    string name;
    Statement *statementBlock;
    Parameters *parameters;
};

class Param{
public:
    Param(string name, DataType type, Expr *expr)
    {
        this->name = name;
        this->type = type;
        this->expr = expr;
    }

    Param(string name, DataType type)
    {
        this->name = name;
        this->type = type;
        this->expr = 0;
    }

    string name;
    DataType type;
    Expr *expr;
};

class DeclarationStatement: public Statement {
    DeclarationStatement(string name, DataType type, Expr *value);
    DeclarationStatement(string name, DataType type);

    void execute() {}
    StatementKind getKind() { return DECLARATION_STATEMENT; }

    string name;
    DataType type;
    Expr *value;
};

#endif

