
#include <cstdio>
#include "ast.h"

map<string, Vvalue> Globalvars;
map<string, Vvalue> vars;
map<string, Vvalue> varsDataType;
map<string, Statement*> functions;

#define IMPLEMENT_RELATIONAL_EVALUATE(CLASS, OP)   \
    Vvalue CLASS::evaluate()            \
    {                                   \
        Vvalue value1 = expr1->evaluate();  \
        Vvalue value2 = expr2->evaluate();  \
        Vvalue result;                      \
                                            \
        result.type = DT_BOOL;               \
        if(value1.type == DT_INT && value2.type == DT_INT){                 \
            result.u.bvalue = value1.ivalue() OP value2.ivalue();       \
        }else{                                                              \
            result.u.bvalue = value1.bvalue() OP value2.bvalue(); \
        }                                                                   \
        return result; \
    }

#define IMPLEMENT_ARITHMETIC_EVALUATE(CLASS, OP)    \
    Vvalue CLASS::evaluate()                \
    {                                       \
        Vvalue value1 = expr1->evaluate();  \
        Vvalue value2 = expr2->evaluate();  \
        Vvalue result;                      \
                                            \
        result.type = DT_INT;                                     \
        result.u.ivalue = value1.ivalue() OP value2.ivalue(); \
        return result;  \
    }

IMPLEMENT_RELATIONAL_EVALUATE(LTExpr, <)
IMPLEMENT_RELATIONAL_EVALUATE(LTEExpr, <=)
IMPLEMENT_RELATIONAL_EVALUATE(GTExpr, >)
IMPLEMENT_RELATIONAL_EVALUATE(GTEExpr, >=)
IMPLEMENT_RELATIONAL_EVALUATE(NEExpr, !=)
IMPLEMENT_RELATIONAL_EVALUATE(EQExpr, ==)

IMPLEMENT_ARITHMETIC_EVALUATE(AddExpr, +)
IMPLEMENT_ARITHMETIC_EVALUATE(SubExpr, -)
IMPLEMENT_ARITHMETIC_EVALUATE(MultExpr, *)
IMPLEMENT_ARITHMETIC_EVALUATE(DivExpr, /)
IMPLEMENT_ARITHMETIC_EVALUATE(ModExpr, %)

Vvalue IdExpr::evaluate(){

    std::map<string, Vvalue>::iterator itLocal;
    itLocal = vars.find(this->id);

    if (itLocal != vars.end()){
        return vars[this->id];
    }

    std::map<string, Vvalue>::iterator itGlobal;
    itGlobal = Globalvars.find(this->id);

    if (itGlobal != Globalvars.end()){
        return Globalvars[this->id];
    }

    throw DecafException("No esta declarada la variable: " + this->id);
}

void BlockStatement::execute()
{
    list<Statement *>::iterator it = this->stList.begin();

    while (it != this->stList.end()) {
        Statement *st = *it;

        st->execute();
        it++;
    }
}

void PrintStatement::execute()
{
    list<Expr *>::iterator it = lexpr->begin();

    while (it != lexpr->end()) {
        Expr *expr = *it;

        Vvalue result = expr->evaluate();

        if(result.type == DT_INT)
            printf("%d\n", result.ivalue());

        if(result.type == DT_HEXA)
            printf("%x\n", result.ivalue());

        if(result.type == DT_BOOL)
            printf("%s\n", result.bvalue() ? "True" : "False" );

        if(result.type == DT_CHAR)
            printf("%c\n", result.cvalue());

        if(result.type == DT_STRING)
            printf("%s\n", result.svalue());

        it++;
    }
}

void AssignStatement::execute()
{
    Vvalue result = expr->evaluate();
    vars[this->id] = result;
    DataType type = result.type;

    if(type != 0)
    {
        varsDataType[this->id] = result;
    }
}

void IfStatement::execute()
{
    Vvalue result = cond->evaluate();

    if(result.type != DT_BOOL)
        return;

    if (result.bvalue()) {
        trueBlock->execute();
    } else if (falseBlock != 0) {
        falseBlock->execute();
    }
}

void WhileStatement::execute()
{
    Vvalue result = cond->evaluate();
    while (result.bvalue())
    {
        statementBlock->execute();
        result = cond->evaluate();
    }
}

void ForStatement::execute()
{
    assignStatement->execute();
    while(cond->evaluate().bvalue()){
        statementBlock->execute();
        finalAssignStatement->execute();
    }
}

void IncStatement::execute()
{
    Vvalue result = vars[id];
    result.u.ivalue++;
    vars[id] = result;
}

void DecStatement::execute()
{
    Vvalue result = vars[id];
    result.u.ivalue--;
    vars[id] = result;
}

VoidStatement::VoidStatement(string name, Statement *statementBlock, Parameters *parameters){
    this->name = name;
    this->statementBlock = statementBlock;
    this->parameters = parameters;

    map<string, Statement*>::iterator it = functions.begin();
    it = functions.find(this->name);
    if(it != functions.end()){
        throw DecafException("Ya esta declarado el metodo: " + this->name);
    }else{
        functions[this->name] = this;
    }
}

FunctionStatement::FunctionStatement(string name, Statement *statementBlock, DataType type, Parameters *parameters)
{
    //map<string, Statement*> functions;
    this->name = name;
    this->statementBlock = statementBlock;
    this->type = type;
    this->parameters = parameters;

    map<string, Statement*>::iterator it = functions.begin();
    it = functions.find(this->name);
    if(it != functions.end()){
        throw DecafException("Ya esta declarada la funcion: " + this->name);
    }else{
        functions[this->name] = this;
    }
}

void VoidStatement::execute()
{
    vars.clear();
    list<Param *>::iterator it = this->parameters->begin();

    while (it != this->parameters->end()) {
        Param *param = *it;

        std::map<string, Vvalue>::iterator itLocal;
        itLocal = vars.find(param->name);

        if (itLocal != vars.end()){
            throw DecafException("Ya esta declarada la variable: " + param->name);
        }else{
            vars[param->name] = param->expr->evaluate();
        }
        it++;
    }
    statementBlock->execute();
    vars.clear();
}


void FunctionStatement::execute()
{
    vars.clear();
    list<Param *>::iterator it = this->parameters->begin();

    while (it != this->parameters->end()) {
        Param *param = *it;

        std::map<string, Vvalue>::iterator itLocal;
        itLocal = vars.find(param->name);

        if (itLocal != vars.end()){
            throw DecafException("Ya esta declarada la variable: " + param->name);
        }else{
            vars[param->name] = param->expr->evaluate();
        }
        it++;
    }
    statementBlock->execute();
    vars.clear();
}


DeclarationStatement::DeclarationStatement(string name, DataType type, Expr *value)
{
    this->name = name;
    this->type = type;
    this->value = value;

    Vvalue result = this->value->evaluate();
    result.type = this->type;

    std::map<string, Vvalue>::iterator it;
    it = Globalvars.find(this->name);

    if (it != Globalvars.end()){
        throw DecafException("Ya esta declarada esta variable" + this->name);
    }else{
        Globalvars[this->name] = result;
    }
}

DeclarationStatement::DeclarationStatement(string name, DataType type)
{
    this->name = name;
    this->type = type;
    this->value = 0;

    Vvalue result;
    result.type = this->type;

    std::map<string, Vvalue>::iterator it;
    it = Globalvars.find(this->name);

    if (it != Globalvars.end()){
        throw DecafException("Ya esta declarada esta variable" + this->name);
    }else{
        Globalvars[this->name] = result;
    }
}













