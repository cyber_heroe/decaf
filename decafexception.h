#ifndef DECAFEXCEPTION_H
#define DECAFEXCEPTION_H

#include <iostream>
#include <string>

using namespace std;

class DecafException
{
public:
    DecafException(const string m = "Exception") : msg(m) { }
    const char* what() { return msg.c_str(); }
private:
    string msg;
};

#endif // DECAFEXCEPTION_H